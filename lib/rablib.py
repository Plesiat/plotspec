

def gsbx(ener, thd, ips, wf, epho=True):
    
    import numpy as np

    if ips:
        if epho:
            sbb = (np.asarray(ener))/wf
        else:
            sbb = (np.asarray(ener)+ips)/wf


        if thd == 0:
            sbx = sbb
        else:
            sbx = np.round(sbb)
            if np.any(np.abs(sbx-sbb) > thd):
                print("Warning! Strange band order detected:", sbb)
            sbx.astype(int)

    else:
        sbx = ener
    return sbx

def esbx(isb, ips, wf, epho=True):
    
    esb = isb*wf
    if not epho:
        esb = isb*wf-ips

    return esb

def posbands(ener, ips, wf, iband=2, epho=True, idx=False, eint=0):
    
    import numpy as np
    import math
    
    ne = len(ener)
    emin, emax = np.min(ener), np.max(ener)
    
    imin = math.ceil(gsbx(emin, 0, ips, wf, epho=epho))
    imax = math.floor(gsbx(emax, 0, ips, wf, epho=epho))
    bord, bpos = [], []
    for i in range(imin, imax+1):
        e = esbx(i, ips, wf, epho=epho)
        idx = (np.abs(ener-e)).argmin()
        if e-eint >= emin and e+eint <= emax:
            if i % 2 == iband-1 or iband == 0:
                if idx:
                    bord.append(i)
                    bpos.append(idx)
                    #print(i,bpos[-1],ener[bpos[-1]])
    
    return bord, bpos


def filtbands(iflims, prob, tper):
    
    import numpy as np
    
    selbands = []
    pthd = np.max(np.sum(prob,axis=0))*tper/100.
    nb = len(iflims[0])
    for j in range(nb):
        testband = False
        for i in range(iflims[0][j],iflims[1][j]):
            if np.sum(prob[:,i]) >= pthd:
                testband = True
        if testband:
            selbands.append(j)
        
    return selbands


def maskbands(bord,selbands,sbor):
    
    newbands = []
    for i in range(len(selbands)):
        if bord[i] in sbor:
            newbands.append(selbands[i])
    
    #for i in range(len(sbor)):
        #try:
            #idx = bord.index(sbor[i])
        #except:
            #idx = None
        #newbands.append(idx)
    
    return newbands

def findbands(ener, ips, wf, iband, eint, epho=True):
    
    import numpy as np
    
    ne = len(ener)
    
    if iband == 3:
        bord, bpos = [0 for i in range(ne)], [i for i in range(ne)]
        benr = [i for i in ener]
        eflims = [benr, benr]
        iflims = [bpos, [i+1 for i in bpos]]
        
    else:
        bord, bpos = posbands(ener, ips, wf, iband=iband, epho=epho, idx=True, eint=eint)
    
        benr = []
        eflims, iflims = [[], []], [[], []]
        for i in bord:
            #benr.append(ener[i])
            benr.append(esbx(i, ips, wf, epho=epho))
            eflims[0].append(benr[-1]-eint)
            eflims[1].append(benr[-1]+eint)
            iflims[0].append(np.argmax(ener>=eflims[0][-1]))
            iflims[1].append(np.argmin(ener<=eflims[1][-1])+1)
        
    
    selbands = list(range(len(bpos)))
    
    return bord, benr, bpos, selbands, eflims, iflims


def fitfunc(x, env, ff, amp, frq, *par):
    
    import numpy as np
    
    pphase = 0.
    if "+p" in ff:
        pphase = np.pi
    elif "-p" in ff:
        pphase = -np.pi
    #print("* Fitting with phase = ", pphase)

    if ff[-1] == "-":
        psign = -1.
    else:
        psign = 1.
    #print("* Fitting with sign = ", psign)

    if env == "gaus":
        tff = np.exp(-par[0]*(x-par[1])**2)
    elif env == "cos2":
        tff = (np.cos(par[0]*(x-par[1])))**2
    else:
        try:
            tff = np.ones(len(x))
        except:
            tff = 1.

    if len(par) == 4:
        if ff[:3] == "cos":
            tff = tff*((amp-par[2])*(np.cos(frq*x+psign*par[3]+pphase))+par[2])
        if ff[:3] == "sin":
            tff = tff*((amp-par[2])*(np.sin(frq*x+psign*par[3]+pphase))+par[2])

    if len(par) == 5:
        if ff[:3] == "cos":
            tff = tff*((amp-par[2])*(np.cos(par[4]*x+psign*par[3]+pphase))+par[2])
        if ff[:3] == "sin":
            tff = tff*((amp-par[2])*(np.sin(par[4]*x+psign*par[3]+pphase))+par[2])
    return tff

def fitbands(tdel,ener,prob,selbands,eflims,iflims,env,ff,frq,ffrq=False,mode="int",fde=0.,doplot=True):
    
    import sys
    import numpy as np
    from scipy.interpolate import interp1d
    from scipy.integrate import fixed_quad
    from scipy.optimize import curve_fit
    from scipy.stats import chisquare
    from lib.tuttilib import wrapnum
    import matplotlib.pyplot as plt
    
    femt2au = 41.341373336
    au2ev = 27.21138505
    cthd = 1e-6
    
    nx = len(tdel)
    ny = len(ener)
    nb = len(selbands)
    
    if nx < 3:
        print("Error! Number of time delays is too low...")
        sys.exit()
    
    if ffrq:
        npar = 5
    else:
        npar = 4
    
    if nb > 0:
        zint = np.zeros((nb,nx))
        popts = np.zeros((nb,npar))
        amp = np.zeros(nb)
        
        xmax = np.max(np.abs(tdel))
        
        if mode == "int":
            for i in range(nx):
                zf = interp1d(ener,np.abs(prob[i,:]), kind='cubic')
                for l in range(nb):
                    zint[l,i] = fixed_quad(zf,eflims[0][selbands[l]],eflims[1][selbands[l]],n=30)[0]
        else:
            for l in range(nb):
                isj, iej = iflims[0][selbands[l]], iflims[1][selbands[l]]
                k = prob[:,isj:iej].sum(axis=0).argmax()
                zint[l,:] = np.abs(prob[:,isj+k])
                print("* l, e0 = ", l, ener[isj+k])
        
        for l in range(nb):
            amp[l] = max(zint[l,:])
            zrat = min(zint[l,0],zint[l,-1])/amp[l]
            if env == "gaus":
                alpha = np.arccos(np.sqrt(zrat))/xmax
            elif env == "cos2":
                alpha = np.arccos(np.sqrt(zrat))/xmax
            else:
                alpha = 0.
                    
            if ffrq:
                popts[l,:] = [alpha, 0., amp[l]/2., 0., frq]
            else:
                popts[l,:] = [alpha, 0., amp[l]/2., 0.]

        if mode == "int" or mode == "max":
            
            print("* Band phases are obtained by fitting functions")
            
            #pbounds = ([-np.inf,-np.inf,-np.inf,-np.pi],[np.inf,np.inf,np.inf,np.pi])
            #if ffrq:
                #pbounds = (pbounds[0]+[-np.pi], pbounds[1]+[np.pi])
            rmset = 0.
            for l in range(nb):
                par0 = popts[l,:]
                #popt, pcov = curve_fit(lambda x, *par0: fitfunc(x, env, ff, amp[l], frq, *par0), tdel, zint[l,:], p0=par0, maxfev=100000, ftol=1e-15, bounds=pbounds)
                try:
                    popt, pcov = curve_fit(lambda x, *par0: fitfunc(x, env, ff, amp[l], frq, *par0), tdel, zint[l,:], p0=par0, maxfev=10000, ftol=1e-15)
                    popt[3] = wrapnum(popt[3], -np.pi, np.pi)
                except:
                    print("Error! Optimal parameters not found!")
                    popt = [None for i in range(npar)]
                #perr = np.sqrt(np.diag(pcov))
                popts[l,:] = popt[:]
                
                #from sklearn.metrics import mean_squared_error
                #rmse = np.sqrt(mean_squared_error(zint[l,:], fitfunc(tdel, env, ff, amp[l], frq, *popt)))
                #rmset += rmse
                #print("* l, rmse = ", l, rmse)
                
            #print(rmset)
            
        elif mode == "fft":
            
            print("* Band phases are obtained by FFT")
            
            from lib.tuttilib import rebatan
                
            ndel = len(tdel)
            
            # nf=None: we take the fft as it is and select the closest point to 2*omega
            # nf=number: we padd zeros to the fft ad integrate over a frequency region defined by smid (really sensitive) 
            nf = None#5000
            
            if nf is None:
                nf = ndel
            else:
                fde = None
            
            td = np.diff(tdel)
            for i in range(1,ndel-1):
                if np.abs(td[i]-td[i-1]) > 1e-8:
                    print("Warning! The grid in time delays does not seem to be uniform!")
            
            tstep = (tdel[1]-tdel[0])*femt2au   # Careful, the grid must be uniform
            nrj = np.fft.fftfreq(nf,d=tstep)*femt2au #*2.*np.pi We don't multiply by 2*pi to keep it in PHz
            petafrq = frq/(2.*np.pi)
            
            pphase = 0.
            if "+p" in ff:
                pphase = np.pi
            elif "-p" in ff:
                pphase = -np.pi
            
            if ff[:3] == "cos":
                pphase = pphase-np.pi

            if ff[-1] == "-":
                psign = -1.
            else:
                psign = 1.
            
            #tlim = min([np.abs(np.min(tdel)), np.abs(np.max(tdel))])
            td = (np.max(tdel)-np.min(tdel))/2.
            t0 = (np.max(tdel)+np.min(tdel))/2.
            wft = np.pi/(2.*td)
            win = np.zeros(nx)
            for i in range(nx):
                win[i] = np.cos((tdel[i]-t0)*wft)**2
            
            zidel = np.zeros(ny)
            spc, ang = np.zeros((ny,nf)), np.zeros((ny,nf))
            for j in range(ny):
                zf = interp1d(tdel,prob[:,j], kind='cubic')
                zidel[j] = fixed_quad(zf,tdel[0],tdel[-1],n=30)[0]/(tdel[-1]-tdel[0]) # Delay integrated spectrum
                
                #zidel[j] = np.mean(prob[:,j])
                #zidel[j] = 0
                
                if fde is None:
                    ftp = np.fft.fft((prob[:,j]-zidel[j])*win,n=nf)
                else:
                    ftp = np.fft.fft(prob[:,j]-zidel[j])
                
                
                spc[j,:] = np.abs(ftp)
                ang[j,:] = psign*np.angle(ftp)-pphase
                #ang[j,:] = np.angle(ftp)
            
            idx = (np.abs(nrj - petafrq)).argmin()
            smid = np.sum(spc[:,idx])*0.65
            ids = []
            for j in (-1, 1):
                k = idx
                while k > 0 and k < nf-1 and np.sum(spc[:,k]) > smid:
                    k = k+j
                ids.append(k)
            
            if fde is None:
                emm = [nrj[ids[0]], nrj[ids[1]]]
            else:
                # We pick the index of 2*omega
                if fde == 0.:
                    emm = [nrj[idx], nrj[idx]]
                    print("* Closest frequency (PHz): ", nrj[idx])
                else:
                    emm = [petafrq-fde, petafrq+fde]
                    print("* Frequency interval (PHz): ", *[i for i in emm])
                
            
            
            #print(ids, emm)
            #print(eflims[0], eflims[1])
            #print(frq, petafrq)
            
            f = open("phase_enr.dat", "w")
            doavg = True
            oph = 0.
            phs = np.zeros(ny)
            for j in range(ny):
                if emm[0] != emm[1]:
                    zf = interp1d(nrj,ang[j,:], kind='cubic')
                    #phs[j] = np.mean(ang[j,ids[0]:ids[1]+1])
                    phs[j] = fixed_quad(zf, emm[0], emm[1], n=30)[0]/(emm[1]-emm[0])
                else:
                    if fde == 0.:
                        phs[j] = ang[j,idx]
                    else:
                        phs[j] = zf(emm[0])
                #phs = psign*phs-pphase
                
                print(ener[j], phs[j], zidel[j], file=f)
                
            f.close()
            
            zi = interp1d(ener,zidel, kind='cubic')
            zf = interp1d(ener,phs*zidel, kind='cubic')     # Weighted sum from Huppert, Phys. Rev. Lett. 2016
            
            #zi = interp1d(ener,zidel*np.cos(phs), kind='cubic')
            #zf = interp1d(ener,zidel*np.sin(phs), kind='cubic') 
            
            for l in range(nb):
                #popts[l,3] = fixed_quad(zf, eflims[0][selbands[l]], eflims[1][selbands[l]], n=30)[0]/(eflims[1][selbands[l]]-eflims[0][selbands[l]])
                
                i = fixed_quad(zi, eflims[0][selbands[l]], eflims[1][selbands[l]], n=30)[0]
                popts[l,3] = fixed_quad(zf, eflims[0][selbands[l]], eflims[1][selbands[l]], n=30)[0]/i
                #popts[l,3] = fixed_quad(zf, eflims[0][selbands[l]], eflims[1][selbands[l]], n=30)[0]/fixed_quad(zi, eflims[0][selbands[l]], eflims[1][selbands[l]], n=30)[0]
                
                #ztot = 0.
                #for j in range(ny):
                    #if j >= iflims[0][selbands[l]] and j <= iflims[1][selbands[l]]:
                        #popts[l,3] = popts[l,3]+phs[j]*zidel[j]
                        #ztot += zidel[j]
                #popts[l,3] /= ztot
            
            if doplot:
                #ins, ine = np.argmax(nrj>=0.), np.argmax(nrj>=2.)
                ins, ine = np.argmax(nrj>=0.5*petafrq), np.argmax(nrj>=2.5*petafrq)
                #ins, ine = ids[0], ids[1]+1
                #ins, ine = 0, nf
                nn = ine-ins
                x = np.repeat(ener,nn).reshape(ny,nn)
                y = np.repeat(nrj[ins:ine],ny).reshape(nn,ny).transpose()
                z = spc[:,ins:ine]
                
                figsize = (18,12)
                plt.close()
                fig, axarr = plt.subplots(3,1,figsize=figsize,gridspec_kw={'height_ratios': [2, 1, 1]})
                plt.subplots_adjust(wspace=0., hspace=0.)
                
                axarr[2].set_xlabel('Energy (eV)')
                axarr[2].set_ylabel('Phase (rad.)')
                axarr[1].set_ylabel('Int. spectrum')
                axarr[0].set_ylabel('Frequency (PHz)')
                
                axarr[0].pcolor(x, y, z)
                #cb = axarr[0].colorbar()
                axarr[0].axhline(y=emm[0],color='gray', linewidth=2.0, linestyle='-')
                axarr[0].axhline(y=emm[1],color='gray', linewidth=2.0, linestyle='-')
                
                for i in range(3):
                    axarr[i].set_xlim([min(ener),max(ener)])
                    for l in range(nb):
                        for j in range(2):
                            axarr[i].axvline(x=eflims[j][selbands[l]],color='black', linewidth=2.0, linestyle='--')
                axarr[1].plot(ener,zidel)
                axarr[2].plot(ener, phs)
                plt.savefig("fitfreqs.png",bbox_inches='tight',dpi=200)
                plt.draw()
                plt.pause(2)
                #plt.show()
        
        apsd = []
        for l in range(nb):
            zf = interp1d(tdel, zint[l,:], kind='cubic')
            apsd.append(fixed_quad(zf,tdel[0],tdel[-1],n=30)[0]/(tdel[-1]-tdel[0]))
        
        return amp, zint, popts, apsd
    
    else:
        print("Error! No selected bands!")
        sys.exit()

def wtdrab(ik, boe, bstr, selbands, popts, apsd, ft, fp, pfac=1., theta=None, phi=None, reb=False, beta="-", alpha="-", clab="-", ioner=0., vi=[0., 0., 0.], vf=[0.,0.,0.], ntheta=0):
    
    import numpy as np
    au2ev = 27.21138505
    deg2rad = np.pi/180.
    
    from lib.tuttilib import rebatan
    
    nb = len(popts)
    if theta is None:
        nang = 0
    else:
        nang = len(theta)
    
    if ik == 0:
        for f in ft, fp:
            if phi is None or ntheta == nang or ntheta == 0:
                if not ioner is None:
                    print("# ip(eV)= ", ioner*au2ev, file=f)
                print("# beta,alpha(deg)= ", beta, alpha, file=f)
                if not phi is None and len(phi) == 1:
                    print("# phi(deg)= ", phi[0]/deg2rad, file=f)
                if not theta is None:
                    print("# "+bstr+"=: "+" ".join([str(boe[j]) for j in selbands]), file=f)
            else:
                nphi = int(nang/ntheta)
                if ntheta*nphi != nang:
                    print("Error! Inconsistent number of angles!")
                    sys.exit()
                print(0, beta, alpha, 0., ioner, file=f)
                print(" ".join(map(str,vi)), file=f)
                print(" ".join(map(str,vf)), file=f)
                print(ntheta, nphi, nb, file=f)
                print(*[0. for l in range(nb)], file=f)
                print(" ".join([str(boe[j]) for j in selbands]), file=f)
                print(*theta[::nphi], file=f)
                print(*phi[:nphi], file=f)
    
    if reb:
        ophp = 0.
        for l in range(nb):
            ophp, popts[l][3] = rebatan(ophp,popts[l][3])
    
    if theta is None:
        if clab != "-":
            print("#", clab, file=ft)
        for l in range(nb):
            print(boe[selbands[l]], popts[l][3]*pfac, file=ft)
            print(boe[selbands[l]], apsd[l], file=fp)
        print(file=ft)
    else:
        if ntheta == nang or ntheta == 0:
            if phi is None or len(phi) == 1:
                for f in ft, fp:
                    print(theta[ik], end=" ", file=f)
            else:
                for f in ft, fp:
                    print(theta[ik], phi[ik], end=" ", file=f)
            
        print(*[popts[l][3]*pfac for l in range(nb)], file=ft)
        print(*apsd, file=fp)
        
