
import numpy as np
import sys

def susage(arglist,strarg):
    import os

    print()
    print("Bad number of arguments:", len(arglist)-1)
    print("Usage: ", os.path.basename(str(arglist[0])), "\x1B[3m",strarg,"\x1B[23m")
    print()
    sys.exit()

def getnlnc(fname,verbose=0,skipcoms=False):
    
    if skipcoms:
        nl = sum(1 for line in open(fname, 'r') if line.split() != [] and line.split()[0] != "#")
        for line in open(fname, 'r').readlines():
            line = line.split()
            if line[0] != "#":
                nc = len(line)
                break
    else:
        nl = sum(1 for line in open(fname, 'r'))
        nc = len(open(fname, 'r').readline().split())
    if verbose == 1:
        print("File: ", fname, " nl=", nl, " nc=", nc)
    return (nl, nc)

def m_getnlnc(fnames,verbose=0,skipcoms=False):
    nl2 = []
    nc2 = []
    for fname in fnames:
        nl, nc = getnlnc(fname,verbose,skipcoms)
        nl2.append(nl)
        nc2.append(nc)
    return (nl2, nc2)

#def readffile(fname):
#    import numpy as np
#    np.set_printoptions(precision=15)
#
#    return (np.array([ map(float,line.replace("D", "E").split()) for line in open (fname , 'r') ]))

def readffile(fname,nh=0,nf=0):
    np.set_printoptions(precision=15)

    nl, nc = getnlnc(fname)
    with open(fname , 'r') as f:
        for i in range(nh):
            tmp = next(f)
        mat = np.array([ list(map(float,next(f).replace("D", "E").split())) for i in range(nl-nh-nf) ])

    return mat

def readiline(fname,iline):

    fline=""
    f = open(fname)
    for i, line in enumerate(f):
        if i == iline-1:
            fline = line
            break
    f.close()

    return fline


#def nearestval(array,value):
    #import numpy as np
    #idx = (np.abs(array-value)).argmin()
    #return array[idx]

def nearval2(array,value):
    np.set_printoptions(precision=15)

    valn = np.sort(np.abs(array[(array>value)]))[0]
    idx = (np.abs(array-valn)).argmin()
    return idx

def printmat(mat,x=[],fname="",mode="w"):

    import os

    if mode != "w" and mode != "a":
        print("Error! Bad option for argument mode!")
        sys.exit()

    nl = len(mat)
    nx = len(x)

    xval = ["" for i in range(nl)]
    if nx > 0:
        if nx != nl:
            print("Error! x vector and mat do not have the same size!")
            sys.exit()
        else:
            for i in range(nl):
                xval[i] = str(x[i])+" "

    if fname != "":
        f = open(fname, mode)
    for i in range(0, nl):
        line = ' '.join(map(str,mat[i,:])).replace("[", "").replace("]", "")
        line=xval[i]+line

        if fname == "":
            print(line)
        else:
            f.write(line+"\n")
    if fname != "":
        f.close()

#def printvec(vec,fname=""):

    #line = ' '.join(map(str,vec[:])).replace("[", "").replace("]", "\n")

    #if fname == "":
        #print line
    #else:
        #with open(fname, "a") as f: f.write(line+'\n')
        #f.close()

def printvec(vec,fobj=""):

    line = ' '.join(map(str,vec[:])).replace("[", "").replace("]", "\n")

    if fobj == "":
        print(line)
    else:
        print(line, file=fobj)


def testreptyp(s,typ="int"):
    try:
        if typ == "int":
            int(s)
        elif typ == "float":
            float(s)
        elif typ == "str":
            str(s)
        else:
            print("Error! Unknown type or type not implemented!")
            sys.exit()
        return True
    except ValueError:
        return False


def cart2sph(xyz):
    np.set_printoptions(precision=15)

    rtp = np.zeros(3)
    xy = xyz[0]**2 + xyz[1]**2
    rtp[0] = np.sqrt(xy + xyz[2]**2)

    if xy == 0.0 and xyz[2] < 0.0:
        rtp[1] = np.pi
    else:
        rtp[1] = np.arctan2(np.sqrt(xy), xyz[2])

    rtp[2] = np.arctan2(xyz[1], xyz[0])
    if rtp[2] < 0.0:
        rtp[2] = rtp[2]+2*np.pi
    return rtp

def m_cart2sph(xyz):
    np.set_printoptions(precision=15)

    rtp = np.zeros(xyz.shape)
    for i in range(0,len(xyz)):
        rtp[i,:] = cart2sph(xyz[i,:])
    return rtp

def sph2cart(rtp):
    np.set_printoptions(precision=15)

    nv = len(rtp)
    xyz = np.zeros(nv)
    if nv == 2:
        xyz[0] = -rtp[0]*np.cos(rtp[1])
        xyz[1] = rtp[0]*np.sin(rtp[1])
    else:
        xyz[0] = rtp[0]*np.sin(rtp[1])*np.cos(rtp[2])
        xyz[1] = rtp[0]*np.sin(rtp[1])*np.sin(rtp[2])
        xyz[2] = rtp[0]*np.cos(rtp[1])

    return xyz

def m_sph2cart(rtp):
    np.set_printoptions(precision=15)

    xyz = np.zeros(rtp.shape)
    for i in range(0,len(rtp)):
        xyz[i,:] = sph2cart(rtp[i,:])
    return xyz

def getgridshape(grid,verbose=0):

    nv = grid.shape
    ndim = len(nv)

    if ( ndim != 2 ):
        print()
        print("Bad dimension for grid array:", ndim)
        print()
        sys.exit()

    nk = [0 for i in range(nv[1])]

    for i in range(0,nv[1]-1):
        nk[i+1] = 1
        while nk[i+1] <= nv[0] and grid[nk[i+1],i] == grid[nk[i+1]-1,i]:
            nk[i+1] += 1

    nk[0] = nv[0]
    nf = 1
    for i in range(nv[1]-2,-1,-1):
        nf = nf*nk[i+1]
        nk[i] = int(nk[i]/nf)

    if verbose == 1:
        print("Grid size:", nk)

    return nk

def coldimap(gridim,cmapstr='RdBu_r'):

    import matplotlib
    from matplotlib import pyplot as plt

    color_dimension = gridim
    minn, maxx = color_dimension.min(), color_dimension.max()
    norm = matplotlib.colors.Normalize(minn, maxx)
    m = plt.cm.ScalarMappable(norm=norm, cmap=cmapstr)
    m.set_array([])

    return m.to_rgba(color_dimension)


def intsimp2d(x1,x2,funk,mod="cart"):

    import os
    from scipy.integrate import simps

    nx = np.zeros(2, dtype=np.int)
    nfunk = np.zeros(2, dtype=np.int)
    nfunk[0], nfunk[1] = funk.shape
    nx[0], nx[1] = len(x1), len(x2)

    if not (nx==nfunk).all():
        print("Error! Inconsistent size between grid and mesh: ")
        print(nx)
        print(nfunk)
        sys.exit()

    fac1 = np.zeros(nx[0])
    if mod == "sph":
        for i in range(nx[0]):
            fac1[i] = np.sin(x1[i])
    else:
        fac1[:] = 1.0

    int1 = np.zeros(nx[0])
    for i in range(nx[0]):
        int1[i] = simps(funk[i,:],x2)
    int2 = simps(int1*fac1,x1)

    return int2


def intsimp3d(x1,x2,x3,funk,mod="cart"):

    import os
    from scipy.integrate import simps

    nx = np.zeros(3, dtype=np.int)
    nfunk = np.zeros(3, dtype=np.int)
    nfunk[0], nfunk[1], nfunk[2] = funk.shape
    nx[0], nx[1], nx[2] = len(x1), len(x2), len(x3)

    if not (nx==nfunk).all():
        print("Error! Inconsistent size between grid and mesh: ")
        print(nx)
        print(nfunk)
        sys.exit()

    fac1 = np.zeros(nx[1])
    fac2 = np.zeros(nx[0])
    if mod == "sph":
        for j in range(nx[1]):
            fac1[j] = np.sin(x2[j])
        for i in range(nx[0]):
            fac2[i] = x1[i]**2
    else:
        fac1[:] = 1.0
        fac2[:] = 1.0

    int1 = np.zeros(nx[1])
    int2 = np.zeros(nx[0])
    for i in range(nx[0]):
        for j in range(nx[1]):
            int1[j] = simps(funk[i,j,:],x3)
        int2[i] = simps(int1*fac1,x2)
    int3 = simps(int2*fac2,x1)

    return int3


def resmooth(xpts,ypts,fac=1.,shift=0.,idx=2,mmax=1):
    
    nk = 4
    dk = np.zeros((nk,4))
    ds = np.zeros(nk)
    for m in range(1,mmax+1):
        dk00 = (ypts[1]-ypts[0])/(xpts[1]-xpts[0])
        dk10 = (ypts[2]-ypts[1])/(xpts[2]-xpts[1])
        dk12 = (ypts[2]*fac+shift-ypts[1])/(xpts[2]-xpts[1])
        dk20 = (ypts[3]-ypts[2])/(xpts[3]-xpts[2])
        dk21 = (ypts[3]-ypts[2]*fac-shift)/(xpts[3]-xpts[2])
        dk22 = (ypts[3]*fac+shift*m-ypts[2])/(xpts[3]-xpts[2])
        dk23 = (ypts[3]*fac+shift*m-ypts[2]*fac-shift)/(xpts[3]-xpts[2])

        ds[0] = ds[0]+np.abs(dk20-dk10)+np.abs(dk10-dk00)  # 0, 1, 2, 3 same signs
        ds[1] = ds[1]+np.abs(dk22-dk10)+np.abs(dk10-dk00) # 0, 1, 2 same signs ; 3 flipped
        ds[2] = ds[2]+np.abs(dk21-dk12)+np.abs(dk12-dk00) # 0, 1, 3 same signs ; 2 flipped
        ds[3] = ds[3]+np.abs(dk23-dk12)+np.abs(dk12-dk00) # 0, 1 same signs ; 2, 3 flipped
    
        if idx == 2:
            if ds[2] < ds[0] and ds[2] < ds[1] or ds[3] < ds[0] and ds[3] < ds[1]:
                ypts[idx] = ypts[idx]*fac+shift
                ypts[idx+1] = ypts[idx+1]*fac+shift*m
                break
    #elif idx == 3:
        #if ds[1] < ds[0] and ds[1] < ds[2]:
            #yval = yval*fac+shift
    
    return ypts


def refubu(xvals,yvals,mmax=1):
    
    from scipy.interpolate import UnivariateSpline
    
    xgrid = np.linspace(xvals[0], xvals[-1], num=1000)
    for i in range(1,len(xvals)):
        dmin, shift = 1e30, 0.
        for s in range(-1,2,2):
            for m in range(mmax+1):
                ynew = np.copy(yvals)
                ynew[i:] = ynew[i:]+m*s*np.pi
                fy = UnivariateSpline(xvals, ynew, k=3, s=0)
                dsum = np.sum(np.abs(fy.derivative()(xgrid)))
                if dsum < dmin:
                    dmin = dsum
                    shift = m*s*np.pi
                #print(xvals[i],s,m,dsum,dmin,m*s*np.pi)
        yvals[i:] = yvals[i:]+shift
    
    return yvals
        

def rebatan2(xpts,ypts,idx=2):
    
    ypts = resmooth(xpts,ypts,shift=np.pi,idx=idx,mmax=3)
    ypts = resmooth(xpts,ypts,shift=-np.pi,idx=idx,mmax=3)
    
    return ypts

def rebatan(ophp,dre,dim=None,div=1.,pinb=False):

    eps = 1e-18

    if dim == None:
        php = dre
    else:
        if np.abs(dre) <= eps:
            if np.abs(dim) <= eps:
                php = 0.
            else:
                if dre > 0.:
                    php = np.pi/2.
                else:
                    php = -np.pi/2.
        else:
            php = np.arctan2(dim,dre)

    #php = php+int(ophp/np.pi)*np.pi
    dphp = php-ophp
    dmin = dphp
    i=1
    s=0
    n=0
    while i<= 200:
        if s == 0:
            for j in range(-1,2,2):
                dtmp = dphp+j*i*np.pi/div
                if abs(dtmp) < abs(dmin):
                    dmin = dtmp
                    n=i
                    s = j
            if s == 0:
                break
        else:
            dtmp = dphp+s*i*np.pi/div
            if abs(dtmp) < abs(dmin):
                dmin = dtmp
                n=i
            else:
                break
        i=i+1

    if i == 201:
        print("Warning!! rebatan not converged!")

    if pinb:
        return n*s

    else:
        php = dmin+ophp
        return php, php


def chimbatan(x, phs):

    for idx in range(2,len(phs)-1):

        d0 = (phs[idx-1]-phs[idx-2])/(x[idx-1]-x[idx-2])

        #print(idx, x[idx], phs[idx-2], phs[idx-1], phs[idx], phs[idx+1])
        dp = []
        for i in range(-2, 3):
            for j in range(-2, 3):
                d1 = (phs[idx]+i*np.pi-phs[idx-1])/(x[idx]-x[idx-1])
                d2 = (phs[idx+1]+j*np.pi-phs[idx]-i*np.pi)/(x[idx+1]-x[idx])
                #print(i, j, d0, d1, d2)
                dp.append(np.abs(d2-d1)+np.abs(d1-d0))

        icase = int(dp.index(min(dp))/5)
        #print(d0, d1, d1-d0, d2)
        #print(icase, dp.index(min(dp)), dp)

        phs[idx] = phs[idx]+(icase-2)*np.pi

    dp = []
    for i in range(-2, 3):
        dp.append(np.abs(phs[-1]+i*np.pi-phs[-2]))
    icase = dp.index(min(dp))
    phs[-1] = phs[-1]+(icase-2)*np.pi

    for idx in range(1,-1,-1):
        dp = []
        for i in range(-2, 3):
            dp.append(np.abs(phs[idx]+i*np.pi-phs[idx+1]))
        icase = int(dp.index(min(dp)))
        phs[idx] = phs[idx]+(icase-2)*np.pi

    return phs

def nheadlines(fname):
    f = open(fname)
    k = 0
    for line in f.readlines():
        line = line.split()
        if line != [] and "#" in line[0]:
            k += 1
        else:
            break

    return k

def hfileform(fname):
    f = open(fname)
    k = []
    for i in range(2):
        k.append(f.readline().split()[0])

    testform = False
    if k.count("#") == 2:
        testform = True
    f.close()

    return testform

def sloadtxt(fname,col=1):

    nh = nheadlines(fname)
    f = open(fname,"r")
    header = ""
    for i in range(nh-1):
        header = header+f.readline()
    k, nl, nc = 0, 0, 0
    dat = [[]]
    coms = []
    fform = True
    f.seek(0)
    for line in f.readlines():
        k += 1
        line = line.split()
        if line != [] and k >= nh:
            if "#" in line[0]:
                coms.append(" ".join(line[1:]))
                if nc != 0:
                    dat.append(tmp)
                nr = 0
                nc += 1
                tmp = []
            elif nc != 0:
                line = [float(i) for i in line]
                nr += 1
                if nc == 1:
                    dat[0].append(line[0])
                    tmp.append(line[col])
                else:
                    if nr > len(dat[nc-2]):
                        print("Error! Inconsistent number of lines in sloadtxt: ", len(dat[nc-2]), nr)
                        sys.exit()
                    if line[0] != dat[0][nr-1]:
                        print("Error! Inconsistent grid in sloadtxt: ", dat[0][nr-1], line[0])
                        sys.exit()
                    tmp.append(line[col])
            else:
                fform = False

    if fform == True:
        dat.append(tmp)
        dat = np.array(dat).T

    if len(coms) == 1:
        fform = False
        header = header+"# "+" ".join(coms)
        coms = []

    return fform, dat, header.rstrip(), coms

def neg2ind(ind,nel):
    
    if ind >= 0:
        return ind
    else:
        if nel == None:
            return ind
        else:
            if not ind.is_integer():
                print("Error! Parameter must be integer in neg2ind function!")
                sys.exit()
            return nel+ind
    

def sequify(sseq,ordu=0,shift=0.,gseq=None,nseq=1,nel=None,ntot=None):

    import math

    if sseq == "-" or sseq == "" or sseq == "all" or sseq == None:
        if gseq is None:
            if nel is None:
                lseq = sseq
            else:
                lseq = [i for i in range(nel)]
        else:
            lseq = gseq

    else:
        aseqs = sseq.split("_")
        nd = len(aseqs)
        lseq = []
        for aseq in aseqs:
            seqs = aseq.split(",")
            fseq = []
            for seq in seqs:
                if seq == "":
                    fseq.append(None)
                else:
                    par = [float(i) for i in seq.split(":")]
                    lpr = len(par)
                    imin = neg2ind(par[0],nel)
                    if lpr == 1:
                        fseq.append(imin+shift)
                    else:
                        imax = neg2ind(par[-1],nel)
                        if lpr == 2:
                            ni = int(imax-imin)+1
                            for i in range(ni):
                                fseq.append(imin+i*1.+shift)
                        elif lpr == 3:
                            if not par[1].is_integer():
                                print("Error! Second parameter must be integer in sequify function!")
                                sys.exit()
                            if par[1] == 1.:
                                fseq.append(imin)
                            else:
                                #if par[1] < 0:
                                    #imax = par[2]
                                st = (imax-imin)/(par[1]-1)
                                for i in range(int(par[1])):
                                    fseq.append(imin+i*st+shift)
                        else:
                            print("Error! Bad format for sequence in sequify function!")
                            sys.exit()

            if abs(ordu) == 1:
                fseq = list(set(fseq))
                if ordu == 1:
                    fseq.sort()
                else:
                    fseq.sort(reverse=True)

       
            if len(fseq) == 1 and nseq != 1:
                fseq = [fseq[0] for i in range(nseq)]

            if nd == 1:
                lseq = fseq
            else:
                lseq.append(fseq)
        
        for i in range(len(fseq)):
           if fseq[i] != None:
               if fseq[i].is_integer():
                   fseq[i] = int(fseq[i])
               if isinstance(fseq[i], float):
                   fseq[i] = float(fseq[i])
        
        if not ntot is None:
            k = len(lseq)
            if k == 1:
                lseq = [lseq[0] for i in range(ntot)]
            elif k != ntot:
                print("Error! Inconsistent dimensions in sequify!")
                sys.exit()

    return lseq


def sliceprocs(rank,size,ngrid,verbose=0):

    if rank == 0 and verbose > 0:
        print()
        print("* Number of processors:", size)

    nrg = np.zeros(size,dtype=int)
    nrg[:] = int(ngrid/size)
    for i in range(size):
        if i < ngrid%size:
            nrg[i] = nrg[i]+1

    if rank == 0:
        lrg = np.arange(0,nrg[0])
    else:
        lrg = np.arange(sum(nrg[:rank]),sum(nrg[:rank+1]))

    if verbose > 1:
        print("* Grid number for proc {}: {}".format(rank,nrg[rank]))
    if verbose > 2:
        print("* Grid points for proc {}: {}".format(rank,*lrg))

    return lrg


def mqdm(lst,rank):

    from tqdm import tqdm

    if rank == 0:
        return tqdm(lst)
    else:
        return lst

def nqdm(lst,rank):

    return lst


def comonstr(slist,sep=None,sep2=None):

    if sep:
        lstring = []
        for s in slist:
            lstring.append(s.split(sep))
    else:
        lstring = slist

    ns = len(lstring)
    outname = lstring[0]
    no = len(outname)
    for i in range(1,ns):
        nu = len(lstring[i])
        if nu != no:
            print("Warning! Inconsistent strings in comonstr!")
            outname = lstring[0]
            break
        for j in range(1,nu):
            if lstring[i][j] != outname[j]:
                if sep2:
                    sub1 = lstring[i][j].split(sep2)
                    sub2 = outname[j].split(sep2)
                    n2 = len(sub2)
                    if len(sub1) == n2:
                        sub3 = []
                        for k in range(n2):
                            if sub1[k] == sub2[k]:
                                sub3.append(sub1[k])
                        outname[j] = sep2.join(sub3)
                    else:
                        outname[j] = "-"
                else:
                    outname[j] = "-"

    if sep:
        outname = sep.join(outname)

    return outname


#def splotrtp(rtp,oname=None,vecs=None):

    #from matplotlib import pyplot as plt
    #import matplotlib as mpl
    #from matplotlib.patches import FancyArrowPatch
    #from mpl_toolkits.mplot3d import proj3d

    #class Arrow3D(FancyArrowPatch):

        #def __init__(self, xs, ys, zs, *args, **kwargs):
            #FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
            #self._verts3d = xs, ys, zs

        #def draw(self, renderer):
            #xs3d, ys3d, zs3d = self._verts3d
            #xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
            #self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
            #FancyArrowPatch.draw(self, renderer)

    #nl = len(rtp)
    #nk = getgridshape(rtp[:,0:2])
    #xyz = np.zeros((nl,3))
    #xyz = m_sph2cart(rtp[:,0:3])

    #x = xyz[:,0].reshape(nk)
    #y = xyz[:,1].reshape(nk)
    #z = xyz[:,2].reshape(nk)

    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #ax.set_xlabel("x")
    #ax.set_ylabel("y")
    #ax.set_zlabel("z")

    #if vecs != None:
        #nv = len(vecs)
        #for i in range(nv):
            #vec = [float(j)*np.max(rtp[:,0])/np.linalg.norm(vecs[i]) for j in vecs[i]]
            #col = plt.cm.ScalarMappable(norm=mpl.colors.Normalize(vmin=0, vmax=nv-1), cmap="tab10").to_rgba(float(i/nv))
            #ax.add_artist(Arrow3D([0, vec[0]], [0, vec[1]], [0, vec[2]], mutation_scale=20,lw=1, arrowstyle="-|>", color=col))

    #cmapstr="cmrmap"
    #cmapstr="gnuplot"
    #cmapstr="autumn_r"
    #fcolors = coldimap(rtp[:,0].reshape(nk),cmapstr)
    #if oname:
        ##ls = LightSource(azdeg=0, altdeg=65)
        ##rgb = ls.shade(z, plt.cm.RdYlBu)
        #ax.plot_surface(x,y,z, rstride=1, cstride=1, alpha=0.5, antialiased=False, facecolors=fcolors)
    #else:
        #ax.plot_surface(x,y,z,facecolors=fcolors, alpha=0.5)
        ##ax.plot_surface(x,y,z,facecolors=fcolors, lw=0.5, alpha=0.5)
        ##ax.contour(x,y,z, 10, lw=3, cmap=cmapstr, linestyles="solid", offset=-1)
        ##ax.contour(x,y,z, 10, lw=3, colors="k", linestyles="solid")


    #if oname:
        #plt.savefig(oname)
    #else:
        #plt.show()

    #plt.close()

def convdat(dat, sigma, emin=None, emax= None, vmin=None, vmax=None, cfilter="gaus", nsimp=None, addp=False):

    from scipy.interpolate import interp1d
    from scipy import integrate

    e1 = min(dat[:,0])+sigma
    e2 = max(dat[:,0])-sigma
    imin = np.argmax(dat[:,0]>=e1)
    imax = np.argmax(dat[:,0]>e2)

    if emin == None:
        emin = e1
    if emax == None:
        emax = e2

    nr, nc = dat.shape
    conv = np.zeros((imax-imin,nc))
    nlist = []
    for c in range(1,nc):
        k = 0
        for i in range(imin,imax):
            conv[k,0] = dat[i,0]
            e1= conv[k,0]-sigma/2.
            e2 = conv[k,0]+sigma/2.

            if cfilter == "gaus":
                filt = np.exp(-0.5*((dat[:,0]-conv[k,0])/sigma)**2.)/sigma
            sig = interp1d(dat[:,0],dat[:,c]*filt, kind='cubic')
            if nsimp == None:
                conv[k,c], err = integrate.quad(sig, e1, e2, limit=200)
            else:
                x = np.linspace(e1,e2,num=nsimp)
                conv[k,c] = integrate.simps(sig(x),x)
            if conv[k,0] < emin or conv[k,0] > emax:
                nlist.append(k)
            if vmin != None and conv[k,c] < vmin:
                nlist.append(k)
            if vmax != None and conv[k,c] > vmax:
                nlist.append(k)
            k += 1

    conv = np.delete(conv,nlist, axis=0)

    if addp:
        for c in range(1,nc):
            #print(conv.shape,dat[0,:].shape,dat[0,:])
            conv = np.insert(conv,0,dat[0,:],0)
            #print(conv.shape,dat[0,:].shape,dat[0,:])

    return conv

def retime(i,ni,rank=0,pref=""):
    import time
    global isrtmx

    #print(rank,i)
    if rank == 0:
        if i == 0:
            isrtmx = time.time()
        else:
            print(pref+"{}/{} - ET(mn): {:.3f} ; RT(mn): {:.3f}\r".format(str(i+1).zfill(4), str(ni).zfill(4), (time.time() - isrtmx)/60., (ni-i-1)*(time.time() - isrtmx)/(60.*(i+1))), end=" ", flush=True)


def ramatgen(u1,u2):

    u1b = u1/np.linalg.norm(u1)
    u2b = u2/np.linalg.norm(u2)
    IM = np.identity(3)
    u3 = np.cross(u1b,u2b)
    sang = np.linalg.norm(u3)

    cang = np.dot(u1b,u2b)
    if sang == 0.:
        fang = np.dot(u1b,u1b)
        if cang == fang:
            RM = IM
        else:
            print("Error! Problem with anticollinear vectors!")
            sys.exit()
    else:
        fang = (1.-cang)/(sang**2)

        vx = np.zeros((3,3))
        vx[0,1] = -u3[2]
        vx[0,2] = u3[1]
        vx[1,0] = u3[2]
        vx[1,2] = -u3[0]
        vx[2,0] = -u3[1]
        vx[2,1] = u3[0]
        RM = IM+vx+vx.dot(vx)*fang

    return RM#.T

def rotmat(u1, u2, c1, c2, c3=None, aform=False):

    RM = ramatgen(u1,u2)

    if isinstance(c1, float):
        c1, c2, c3 = [c1], [c2], [c3]

    nc = len(c1)
    if nc != len(c2):
        print("Error! Inconsitent array sizes!")
        sys.exit()
    if c3 is None:
        from tuttilib import sph2cart, cart2sph
        tp = np.zeros((nc,2))
        for i in range(nc):
            xyz = sph2cart([1.,c1[i],c2[i]])
            xyz = RM.T.dot(xyz)
            r, tp[i,0], tp[i,1] = cart2sph(xyz)
        if aform:
            return tp
        else:
            return tp[:,0], tp[:,1]
    else:
        if nc != len(c3):
            print("Error! Inconsitent array sizes!")
            sys.exit()
        xyz = np.zeros((nc,3))
        for i in range(nc):
            kk = np.array([c1[i],c2[i],c3[i]])
            xyz[i,:] = RM.dot(kk)
        if aform:
            return xyz
        else:
            return xyz[:,0], xyz[:,1], xyz[:,2]

def intphi(phi, rval, no=30):

    import numpy as np
    from scipy.integrate import fixed_quad
    from scipy.interpolate import interp1d

    nphi = len(phi)
    ntheta = int(len(rval)/nphi)
    rtet = np.zeros(ntheta)
    for i in range(ntheta):
        fz = interp1d(phi, rval[i*nphi:(i+1)*nphi], kind='cubic')
        rtet[i] = fixed_quad(fz, phi[0], phi[-1], n=no)[0]
    return rtet

def inttet(theta, rtet, no=30):

    import numpy as np
    from scipy.integrate import fixed_quad
    from scipy.interpolate import interp1d

    fz = interp1d(theta,rtet*np.sin(theta), kind='cubic')
    tint = fixed_quad(fz, theta[0], theta[-1], n=no)[0]
    return tint

def intsph(theta, phi, rval, no=30):

    nphi = len(phi)
    rtet = intphi(phi, rval, no=no)
    tint = inttet(theta, rtet, no=no)

    return tint

def formplot(fsz,xstp,xmin,xmax,ystp,ymin,ymax,sx=8,sy=11):

    import matplotlib.pyplot as plt

    params = {'legend.fontsize': fsz,
            'axes.labelsize': fsz*1.4,
            'axes.titlesize': fsz*1.4,
            'xtick.labelsize' :fsz,
            'ytick.labelsize': fsz,
            'grid.color': 'k',
            'grid.linestyle': ':',
            'grid.linewidth': 0.5,
            'mathtext.fontset' : 'custom',
            'mathtext.rm'      : 'FreeSerif',
            'font.family'      : 'FreeSerif',
            'font.serif'       : 'FreeSerif',
            }
    plt.rcParams.update(params)
    fig, ax = plt.subplots(1,figsize=(sx,sy))
    plt.xlim(xmin,xmax)
    plt.ylim(ymin,ymax)

    if xstp != 0:
        if xmax%xstp == 0:
            plt.xticks(np.arange(xmin, xmax+xstp, step=xstp))
        else:
            plt.xticks(np.arange(xmin, xmax, step=xstp))
    if ystp != 0:
        if ymax%ystp == 0:
            plt.yticks(np.arange(0., ymax+ystp, step=ystp))
        else:
            plt.yticks(np.arange(0., ymax, step=ystp))

    return plt, fig, ax

def getcoms(fname):
    coms = ""
    f = open(fname)
    line = f.readline()
    while line.split() != [] and line.split()[0] == "#":
        coms = coms+line
        line = f.readline()
    f.close()

    return coms.rstrip()

def getcvals(fname, ny=-2, cont_only=False, full_header=False):

    header, y, lab, oline = "", None, "", ""
    f = open(fname)
    while True:
        iline = f.readline()
        sline = iline.split()
        if sline == []:
            break
        if sline[0] == "#":
            header = header+oline
            if len(sline) == ny and not isinstance(sline[1], str):
                lab = ""
                y = np.array(sline[1:], dtype=float)
                break
            elif len(sline) == ny+1:
                lab = sline[1]
                y = np.array(sline[2:], dtype=float)
            else:
                oline = iline
                lab = sline[1]
                if not isinstance(sline[2], str):
                    y = np.array(sline[2:], dtype=float)
                
        elif cont_only:
            break
    f.close()
    
    if full_header:
        header = header+iline

    return header.strip(), y, lab


def readpad(fname, col=0, pack=False):

    line = open(fname).readline().split()[0]
    if line[0] == "#":
        if col == 0:
            dat = np.loadtxt(fname)
        else:
            dat = np.loadtxt(fname, usecols=(0,1,col+1))
        if not pack:
            ntheta, nphi = getgridshape(dat[:,:2])
            theta, phi = dat[::nphi,0], dat[:ntheta,1]
            rval = dat[:,2:]
            spad = [ntheta, nphi, len(rval[0])]
            hpar1, hpar2 = [getcoms(fname).split()], []

    else:
        f = open(fname)
        hpar1 = []
        for i in range(3):
            hpar1.append([float(i) for i in f.readline().split()])

        ntheta, nphi, nc = [int(i) for i in f.readline().split()]
        if 1 > col > nc:
            print("Error! Bad option for col index in readpad!")
            sys.exit()

        if col == 0:
            ics, ice = 0, nc
            ns = np.copy(nc)
        else:
            ics, ice = col-1, col
            ns = 1
        spad = [ntheta, nphi, ns]

        hpar2 = []
        for i in range(2):
            hpar2.append(f.readline())
        nt = ntheta*nphi
        theta, phi, rval = np.zeros(ntheta), np.zeros(nphi), np.zeros((nt,ns))
        theta[:] = np.array([float(i) for i in f.readline().split()])
        phi[:] = np.array([float(i) for i in f.readline().split()])
        for i in range(nt):
            rval[i,:] = [float(j) for j in f.readline().split()][ics:ice]
        f.close()

        if pack:
            dat = np.zeros((nt,ns+2))
            k = -1
            for i in range(ntheta):
                for j in range(nphi):
                    k += 1
                    dat[k,0] = theta[i]
                    dat[k,1] = phi[j]
                    dat[k,2:] = rval[k,:]

    if pack:
        return dat
    else:
        return spad, theta, phi, rval, hpar1, hpar2



def writepad(fname, spad, theta, phi, rval, hpar1, hpar2):

    nt = spad[0]*spad[1]
    f = open(fname, "w")
    for hpar in hpar1:
        print(*hpar, file=f)

    print(hpar2)
    if hpar2 == []:
        k = -1
        for i in range(spad[0]):
            for j in range(spad[1]):
                k += 1
                print(theta[i], phi[j], *rval[k,:spad[2]], file=f)
    else:
        print(*spad, file=f)
        for hpar in hpar2:
            print(*hpar, file=f)

        print(*theta, file=f)
        print(*phi, file=f)
        for i in range(nt):
            print(*rval[i,:spad[2]], file=f)
    f.close()

def wrapnum(val, vmin, vmax):
    diff = vmax-vmin
    return (((val - vmin) % diff) + vmin)



def get_ttwkb(fv,xdat,ydat,e):
    
    from scipy.optimize import fsolve
    from scipy.integrate import fixed_quad
    
    au2ev = 27.21138505
    atto2au = 41.341373336/1000.
    nr = len(xdat)
    
    xi = []
    for i in range(1,nr):
        if (ydat[i-1]-e)*(ydat[i]-e) < 0:
            xi.append(xdat[i])
            break
    for i in range(nr-2,-1,-1):
        if (ydat[i+1]-e)*(ydat[i]-e) < 0:
            xi.append(xdat[i])
            break
    if len(xi) != 2:
        print("Warning! Inconsistent size of xi: e, xi = ", e, xi)
        prob, time = np.nan, np.nan
    else:
        #print(xi)
        for i in range(2):
            xi[i] = fsolve(lambda x: fv(x)-e,xi[i])[0]
        #print(e, *xi)
        
        #fac = 4.*np.sqrt((emax-e)*(e-emin))/de
        #trans = np.exp(-2*fixed_quad(lambda x: np.sqrt(2.*(fv(x)-e)), xi[0], xi[1], n=30)[0])
        
        fac = np.exp(-2*fixed_quad(lambda x: np.sqrt(2.*(fv(x)-e)), xi[0], xi[1], n=30)[0])
        trans = 1./(1+0.25*fac)**2
        
        prob = fac*trans
        
        fac = 1.
        trans = fixed_quad(lambda x: 1./np.sqrt(2.*(fv(x)-e)), xi[0], xi[1], n=30)[0]
        
        time = fac*trans/atto2au
        
        ##fac = 1.
        #trans = np.exp(2.*fixed_quad(lambda x: np.sqrt(2.*(fv(x)-e)), xi[0], xi[1], n=30)[0])
        ##fac = fixed_quad(lambda x: 2./fv(x), xmin, xi[0], n=30)[0]
        #fac = (xi[0]-xmin)/np.sqrt(2*e)
        
    return prob, time
