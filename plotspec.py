#!/usr/bin/python3

import numpy as np
import argparse
import sys
import os
import os.path
import matplotlib.pyplot as plt
from matplotlib import ticker
from matplotlib.colors import LogNorm
import matplotlib.patches as patches
#from matplotlib import gridspec
#from scipy.optimize import curve_fit, minimize_scalar
from scipy.interpolate import interp1d, interp2d, splrep, sproot, splev
from scipy.integrate import fixed_quad
from scipy.signal import find_peaks, savgol_filter
from lib.tuttilib import sequify, rebatan, hfileform
from lib.rablib import *
import re
import time

parser = argparse.ArgumentParser()
parser.add_argument("fnames", type=str, help="psig1 + psig2 - psig3...", nargs="+")
parser.add_argument("--cols", type=str, help="Columns to be plotted (c1,c2,... ; default=1)")
parser.add_argument("--xmin", type=float, default=None, help="xmin")
parser.add_argument("--xmax", type=float, default=None, help="xmax")
parser.add_argument("--ymin", type=float, default=None, help="ymin")
parser.add_argument("--ymax", type=float, default=None, help="ymax")
parser.add_argument("--vmin", type=float, default=None, help="vmin (colorbar min)")
parser.add_argument("--vmax", type=float, default=None, help="vmax (colorbar max)")
parser.add_argument("--facs", type=str, help="Multiplication factor (fac1,fac2,...)")
parser.add_argument("--gfac", type=float, help="Global multiplication factor")
parser.add_argument("--sgf", type=float, default=0.0, help="Smoothing parameter of the Savitzky-Golay filter")
parser.add_argument("--pplt", type=str, help="Parameters for pulse plotting (E0(eV),A0(eV),W0(eV),Phase(Pi)")
parser.add_argument("--eint", type=str, default=None, help="Energy range for sideband integration (eV) (e0+/-eint)")
parser.add_argument("--fde", type=float, default=0.0, help="Frequency range for sideband integration (PHz) (f0+/-fde)")
parser.add_argument("--auto", action='store_true', help="Select automatically the sidebands")
parser.add_argument("--iband", type=int, default=1, help="Type of bands to be fitted (1=SB/2=MB/0=both/3=all energies)")
parser.add_argument("--tper", type=float, default=0., help="Tolerance for band fittings (in %% of the maximum)")
parser.add_argument("--sbor", type=str, default="0", help="Sequence of band orders to be fitted")
parser.add_argument("--wlf", type=float, help="Wavelength of fundamental pulse (nm)")
parser.add_argument("--ff",  type=str, default="cos+p-", help="Fitting function (cos, sin, cos+p, sin+p)")
parser.add_argument("--env",  type=str, default="gaus", help="Enveloppe function (gaus/cos2/none)")
parser.add_argument("--ffrq", action='store_true', help="Use frequency as a free parameter during fitting")
parser.add_argument("--pso", action='store_true', help="Printing phases as a fuction of the sideband order")
parser.add_argument("--phs", action='store_true', help="Printing phases instead of time delay")
parser.add_argument("--amp", action='store_true', help="Printing amplitudes of sidebands")
parser.add_argument("--reb", action='store_true', help="Rebuild the phase")
parser.add_argument("--ips", type=str, help="Ionization potentials (IP1,IP2,... (eV))")
parser.add_argument("--ipmin", type=float, default=0, help="Manual ionization potential (eV)")
parser.add_argument("--nintx", type=int, help="Number of interpolation points along x axis")
parser.add_argument("--ninty", type=int, help="Number of interpolation points along y axis")
parser.add_argument("--fzeros",action='store_true', help="Extrapolate photoelectron spectra with zeros when using ips")
parser.add_argument("--log", action='store_true', help="Plot the contourplot in log scale")
parser.add_argument("--png", action='store_true', help="Save plots to png file")
parser.add_argument("--fsz", type=float, default=25, help="Font size")
parser.add_argument("--xstp", type=float, default=0, help="xticks step for ax 0")
parser.add_argument("--ystp", type=float, default=0, help="yticks step for ax 0")
parser.add_argument("--xstp1", type=float, default=0, help="xticks step for ax 1")
parser.add_argument("--nzbn", type=float, default=0, help="Number of zticks bins")
parser.add_argument("--epho", action='store_true', help="Put y axis in photon energy (eV)")
parser.add_argument("--emom", action='store_true', help="Put y axis in electron momentum (a.u.)")
parser.add_argument("--ndcb", type=str, default="0", help="Number of digits for color bar ticks")
parser.add_argument("--isx", type=int, default=0, help="Starting index of x coordinates (for ploting)")
parser.add_argument("--ssx", type=int, default=1, help="Step of x coordinates (for ploting)")
parser.add_argument("--isy", type=int, default=0, help="Starting index of y coordinates (for ploting)")
parser.add_argument("--ssy", type=int, default=1, help="Step of y coordinates (for ploting)")
parser.add_argument("--fmode", type=str, default="int", help="Mode for sidebands fitting (int/max/fft)")
parser.add_argument("--notitle", action='store_true', help="Do not print title")
parser.add_argument("--noplot", action='store_true', help="Do not plot anything")
parser.add_argument("--axflip", action='store_true', help="Flip the X and Y axes")
parser.add_argument("--cbvert", action='store_true', help="Place the color bar vertically")
parser.add_argument("--nfft", type=int, help="Number of points for Averaged Fast Fourier Transform of Photoelectron spectra")
parser.add_argument("--exp", action='store_true', help="Export operated photoelectron spectra as ASCII file")
parser.add_argument("--wsel", type=str, default=None, help="Extract values for selected time delay index (avg/0/1/...) If wlf, extract it also as a function of main/sidebands")
parser.add_argument("--cmap", type=str, default="hot", help="Color map for 2d plots (hot/jet/gray/gnuplot/spring/summer/winter/autumn/cool/bone/ocean/gist_stern/cubehelix)")
parser.add_argument("--icomp", type=int, default=None, help="Plot 1d components of the spectra relative to the ith label in filename")
parser.add_argument("--intcol", type=str, default=None, help="Integrate the columns (flat/sin/cos)")
parser.add_argument("--zsym", action='store_true', help="Make zmin=zmax")
parser.add_argument("--trim", action='store_true', help="Trim the exported png file")
parser.add_argument("--loadall", action='store_true', help="Load all the data")
parser.add_argument("--title", type=str, default=None, help="Customize the main part of figure title")
parser.add_argument("--oname", type=str, default=None, help="Customize the title and the outputname by providing a file name")
args = parser.parse_args()

figsize = (18,12)

au2ev = 27.21138505
gauslim = 60.             # Defines where gaussian is topping (10=fwtm)
femt2au = 41.341373336
wl2ener = 45.563352
deg2rad = np.pi/180.

opdic = {'+' : np.add, '-' : np.subtract, '*' : np.multiply, '/' : np.divide, '%' : np.mod}

dthd = 0.05
ethd = 0.01

doint = False
dointx = False
dointy = False
dointw = False

class FormatScalarFormatter(ticker.ScalarFormatter):
    def __init__(self, fformat="%1.1f", offset=True, mathText=True):
        self.fformat = fformat
        ticker.ScalarFormatter.__init__(self,useOffset=offset,
                                                        useMathText=mathText)
    def _set_format(self, vmin, vmax):
        self.format = self.fformat
        if self._useMathText:
            self.format = '$%s$' % ticker._mathdefault(self.format)

def str2symb(sstr):
    
    if sstr != "Total":
    
        l = list(re.search("(^[0-9]*)([a-z]*)([gu]?[0-9]?\.?[0-9]?)",sstr).group(1,2,3))
        
        sstr = ""
        for i in ("sigma", "pi", "delta", "alpha", "beta"):
            if i in l[1]:
                if l[1][-1] in ("g", "u"):
                    l[2] = l[1][-1]+l[2]
                    l[1] = l[1][:-1]
                sstr = r'${}\{}$'.format(l[0],l[1])
                
        if sstr == "":
            sstr = l[0]+l[1]
        
        if l[2] != "":
            sstr = sstr+r'$_{}{}{}$'.format("{",l[2],"}")
    return sstr
        

def plotinit():

    params = {
        'axes.labelsize': args.fsz*1.2,
        'axes.titlesize': args.fsz*1.2,
        'axes.titleweight': "bold",
        'legend.fontsize': args.fsz*0.7,
        'xtick.labelsize' :args.fsz,
        'ytick.labelsize': args.fsz,
        'grid.color': 'k',
        'grid.linestyle': ':',
        'grid.linewidth': 0.5,
        'mathtext.fontset' : 'custom',
        'mathtext.rm'      : 'FreeSerif',
        'font.family'      : 'FreeSerif',
        'font.serif'       : 'FreeSerif',
        }
    plt.rcParams.update(params)
    plt.rc('text.latex', preamble=r'\usepackage{amsmath}')
    #plt.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]

    if comp1d:
        if args.axflip:
            fig, axarr = plt.subplots(2,1,figsize=figsize, gridspec_kw={'height_ratios': [1, 2]})
            axarr = [axarr[1], axarr[0]]
        else:
            fig, axarr = plt.subplots(1,2,figsize=figsize, gridspec_kw={'width_ratios': [2, 1]})
        axarr[1].set_zorder(1)
    else:
        fig, axarr = plt.subplots(1,1,figsize=figsize)
        axarr = [axarr]
    plt.subplots_adjust(wspace=0., hspace=0., bottom=0., top=0.75)
    nax = len(axarr)
    
    xlabel = "Delay (fs)"
    ylabel = "Photoelectron energy (eV)"
    if emom:
        ylabel = "Electron momentum (a.u.)"
    elif epho:
        ylabel = "Photon energy (eV)"
    
    
    if args.axflip:
        dat = [yarr[args.isx::args.ssx,args.isy::args.ssy], xarr[args.isx::args.ssx,args.isy::args.ssy], zarr[args.isx::args.ssx,args.isy::args.ssy]]
    else:
        dat = [xarr[args.isx::args.ssx,args.isy::args.ssy], yarr[args.isx::args.ssx,args.isy::args.ssy], zarr[args.isx::args.ssx,args.isy::args.ssy]]
    
    axax = [("x", "y"), ("y")]
    for i in range(nax):
        for a in axax[i]:
            amin, amax = globals()[a+"min"], globals()[a+"max"]
            getattr(axarr[i], "set_"+ad[a]+"lim")(amin, amax)
            if i == 0:
                s = getattr(args, a+"stp")  # args.xstp
                if s != 0:
                    k = 0
                    if globals()[a+"max"]%s == 0:
                        k = s
                    getattr(axarr[i], "set_"+ad[a]+"ticks")(np.arange(amin, amax+1e-15, step=s))
                getattr(axarr[i], "set_"+ad[a]+"label")(locals()[a+"label"])
            else:
                getattr(axarr[i], "set_"+ad[a]+"ticks")([])
    
    if comp1d:
        zcmin, zcmax = np.min(zcomp[args.isy::args.ssy,:]), np.max(zcomp[args.isy::args.ssy,:])
        if np.abs(zcmin) > zcmax:
            i, j = -1, zcmax/np.abs(zcmin)
            zcmax = np.abs(zcmin)
        else:
            i, j = 0, 1
        s = args.xstp1
        if s != 0:
            k = 0
            if 1%s == 0:
                k = s
            getattr(axarr[1], "set_"+ad["x"]+"ticks")(np.arange(i+s, j+1e-15, step=s))
            
        getattr(axarr[1], "set_"+ad["x"]+"label")("Arb. units")
    
    
    ypos = 0.95
    if ips and args.wlf:
        
        nsi = gsbx(ymin, 0, ipmin, wf, epho=epho)
        nsf = gsbx(ymax, 0, ipmin, wf, epho=epho)

        j = []
        for i in range(int(np.ceil(nsi)//2*2),int(np.ceil(nsf)//2*2),2):
            j.append(i)
        
        ax2 = getattr(axarr[nax-1], "twin"+ad["x"])() #ax2 = ax.twinx()
        getattr(ax2, "set_"+ad["y"]+"ticks")(j)  #ax2.set_yticks(j)
        getattr(ax2, "set_"+ad["y"]+"lim")(nsi, nsf) #ax2.set_ylim(nsi, nsf)
        lab = "Band order"
        if args.iband == 1:
            lab = "Sideband order"
        elif args.iband == 2:
            lab = "Mainband order"
        getattr(ax2, "set_"+ad["y"]+"label")(lab) #ax2.set_ylabel(lab)
        axarr[nax-1].set_zorder(10)
        
        if args.axflip:
            ypos = 1.
    
    if not args.notitle:
        i = str2symb(fldname[4])
        title = "\n"
        if i != "-" and i != "":
            title += "Channel: "+i+r'$^{-1}$'
        title += tlab
        if fldname[8] != "-":
            title += "\nPulse: "+fldname[8]
        if "int" in fldname[3]:
            j = r'   <$\beta$,$\alpha$>'
        else:
            j = r'   ($\beta$,$\alpha$) = ('+re.sub(r'(-*\d+\.*\d*)(-)', r'\1,',fldname[3])+")"
        title += j
        
        ypos = ypos+(title.count("\n")-3)*0.03
        
        if args.title:
            fig.suptitle(args.title+title, fontsize=args.fsz*1.2, y=ypos, weight="bold")
        else:
            fig.suptitle("Basis: "+fldname[0]+"   Gauge: "+fldname[2]+title, fontsize=args.fsz*1.2, y=ypos, weight="bold")
    

    if args.log:
        pcol = axarr[0].pcolor(*dat, cmap=args.cmap, norm=LogNorm(vmin=vmin, vmax=vmax))
        cb = axarr[0].colorbar()
    else:
        pcol = axarr[0].pcolor(*dat, cmap=args.cmap, vmin=vmin, vmax=vmax)#, shading='auto')
        #axarr[0].pcolor(xarr[args.isx::args.ssx,args.isy::args.ssy], yarr[args.isx::args.ssx,args.isy::args.ssy], zarr[args.isx::args.ssx,args.isy::args.ssy], cmap=args.cmap, vmin=vmin, vmax=vmax)
    
        cbor = "horizontal"
        cpad = 0.15
        if args.cbvert:
            cbor = "vertical"
            cpad = 0.05
            if not args.axflip and ips:
                cpad = 0.15
        if args.ndcb != "0":
            cbar_num_format = FormatScalarFormatter("%."+args.ndcb+"f")
            cb = fig.colorbar(pcol, format=cbar_num_format, orientation=cbor, pad=cpad)
        else:
            cb = fig.colorbar(pcol, ax=axarr, orientation=cbor, pad=cpad, shrink=0.6)

        if args.nzbn != 0:
            tick_locator = ticker.MaxNLocator(nbins=args.nzbn)
            cb.locator = tick_locator
            cb.update_ticks()
        cb.formatter.set_powerlimits((0, 0))
    
    cb.update_ticks()
    
    
    #cb.set_label("Probability density", labelpad=args.fsz/2)
    
    if comp1d:
        ncomp = len(zlabs)
        if args.axflip:
            #axarr[1].plot(yarr[0,args.isy::args.ssy], zcomp[args.isy::args.ssy,:]/np.max(zcomp[args.isy::args.ssy,:]))
            for i in range(ncomp):
                axarr[1].plot(yarr[0,args.isy::args.ssy], zcomp[args.isy::args.ssy,i]/zcmax, label=str2symb(zlabs[i]))
        else:
            for i in range(ncomp):
                axarr[1].plot(zcomp[args.isy::args.ssy,i]/zcmax, yarr[0,args.isy::args.ssy], label=str2symb(zlabs[i]))
        axarr[1].legend(loc='upper right')
        if ncomp != 1:
            axarr[1].lines[0].set_linewidth(4.)

    #axarr[0].text((xmax-xmin)/2,0.9*(ymax-ymin), fldname[4], color="white")

    if pplt:
        ppar = [float(i) for i in pplt.split(",")]
        ppar[2] = (femt2au*ppar[2])/au2ev
        #print(ppar)
        axarr[0].plot(xarr[:,0],ppar[0]+ppar[1]*np.sin(ppar[2]*xarr[:,0]+ppar[3]*np.pi), color="black", linewidth=4.0, linestyle='-')

    fig.canvas.set_window_title(outname)

    return fig, axarr


def drawsb(j,iax=0,col="gray"):

    global rects
    
    if args.axflip:
        rects[iax].append(patches.Rectangle((eflims[0][j], xmin), eflims[1][j]-eflims[0][j], xmax-xmin,alpha=0.5,facecolor=col))
    else:
        rects[iax].append(patches.Rectangle((xmin, eflims[0][j]), xmax-xmin,eflims[1][j]-eflims[0][j],alpha=0.5,facecolor=col))
    axarr[iax].add_patch(rects[iax][-1])
    plt.draw()

def on_click(event):

    global selbands, rects

    if event.button == 3:
        selbands = []
        for rect in rects[0]:
            rect.remove()
        plt.draw()
        rects[0] = []

    if event.button == 1:
        try:
            rects[0]
        except:
            rects[0] = []
        
        edata = getattr(event, ad["y"]+"data")
        for j in range(nfl):
            if edata >= eflims[0][j] and edata <= eflims[1][j]:
                if j not in selbands:
                    selbands.append(j)
                    drawsb(j)


if args.pplt:
    pplt = args.pplt
else:
    pplt = None

if args.nfft:
    nfft = args.nfft
else:
    nfft = None

if args.wlf:
    wf = au2ev*wl2ener/args.wlf
    frq = 2.*wf*femt2au/au2ev
    print("* Frequency IR: ", wf, "eV")
    print("* Frequency SB: ", frq/(2.*np.pi), "PHz")
    print("* Period: ", 2.*np.pi/frq, "fs")

if args.eint != None:
    if not args.wlf:
        print("Error! wlf argument must be defined when using eint option!")
        sys.exit()
    eint = float(args.eint)
    #args.epho = True
else:
    eint = None

if args.ff:
    ff = args.ff
else:
    ff = "cos+p-"

i = False
for fname in args.fnames:
    for op in opdic.keys():
        if fname == op:
            i = True

if i == True:
    if len(args.fnames) % 2 == 0:
        print("Error! The number of mandatory arguments must be odd!")
        sys.exit()

    fnames = args.fnames[0::2]
    ops = ["+"]
    ops = ops+args.fnames[1::2]

    if not all(op in list(opdic.keys()) for op in ops):
        print("Error! Some operators are incorrectly defined!")
        sys.exit()

else:
    print("* Summing all files...")
    # Case where only filenames are defined (then assuming addition)
    fnames = args.fnames
    ops = ["+" for fname in args.fnames]

sbor = sequify(args.sbor)

if args.tper and sbor != [0]:
    print("Error! tper and sbor cannot be defined at the same time!")
    sys.exit()
    

nf = len(fnames)
ncm = 1
if args.cols:
    cols = sequify(args.cols)
    ncm = len(cols)
    if ncm != 1:
        if  args.tper:
            print("Error! tper must be equal to zero when using col option!")
            sys.exit()
    else:
        if cols == [0]:
            f = open(fnames[0])
            for line in f.readlines():
                line = line.split()
                if line[0] != "#":
                    ncm = len(line)-1
                    break
            if ncm == 1:
                print("Error! The file contains only one column!")
                sys.exit()
            cols = [i+1 for i in range(ncm)]
        
else:
    cols = [1 for i in range(nf)]

if not args.intcol is None:
    args.loadall = True

if args.epho:
    epho = True
    estr = "ep."
else:
    epho = False
    estr = "ee."

if args.ips:
    ips = [float(i) for i in args.ips.split(",")]

    if len(ips) == 1:
        if nf != 1:
            ips = [ips[0] for i in range(nf)]
    elif len(ips) != nf:
        print("Error! Inconsistent number of IPs!")
        sys.exit()
    dointw = True
else:
    ips = []
    for fname in fnames:
        nform = hfileform(fname)
        if nform:
            ips.append(float(open(fname).readline().split()[1])*au2ev)
    if len(ips) == 0:
        ips = None
    elif len(ips) != nf:
        print("Error! Inconsistent format of files!")
        sys.exit()

if ips == None:
    if epho:
        print("Error! IPs must be defined when using epho option!")
        sys.exit()
    ioner = None
    ipmin = args.ipmin
else:
    
    if args.ipmin == 0:
        ipmin = np.min(ips)
    else:
        ipmin = args.ipmin
    
    ioner = list(set(ips))
    if len(ioner) == 1:
        ioner = ioner[0]/au2ev
    else:
        if len(ips) == nf:
            if epho:
                dointw = True
            else:
                if args.ipmin == 0 and eint:
                    print("Error! ipmin is required when differents IPs are involved, eint is selected and epho is not selected!")
                    sys.exit()
                    
        ioner = None
    
if args.facs:
    facs = [float(i) for i in args.facs.split(",")]
    if len(facs) == 1:
        facs = [facs[0] for i in range(nf)]
    elif len(facs) != nf:
        print("Error! Inconsistent number of facs!")
        sys.exit()
else:
    facs = [1. for i in range(nf)]

if args.gfac:
    facs = [i*args.gfac for i in facs]

if args.emom:
    if args.epho or args.ips:
        print("Error! Cannot use epho or ips argument with emom!")
        sys.exit()
    else:
        emom = True
else:
    emom = False
    
if args.iband == 1:
    btstr = "sb"
elif args.iband == 2:
    btstr = "mb"
else:
    btstr = ""

comp1d = None
if args.icomp:
    comp1d = args.icomp

if args.wsel != None:
    if args.wsel == "avg":
        wsel = ["avg"]
    else:
        wsel = sequify(args.wsel)
    nws = len(wsel)
    fws = [0 for i in range(nws)]
    
    if comp1d != None and nws != 1:
        print("Error! Cannot select several wavepackets with comp1d option!")
        sys.exit()
else:
    wsel = None
    if args.icomp:
        print("Warning! icomp option has been deactivated because option wsel is not defined!")
        comp1d = None
        
if args.axflip:
    ad = {"x": "y", "y": "x"}
else:
    ad = {"x": "x", "y": "y"}

if args.cmap not in ("seismic_r", "hot", "jet", "gray", "gnuplot", "spring", "summer", "winter", "autumn", "cool","bone","ocean","gist_stern","cubehelix"):
    print("Error! Bad option for argument cmap!")
    sys.exit()

doplot = True
if args.noplot:
    doplot = False

nxi = None
if args.nintx:
    nxi = args.nintx
    dointx = True

nyi = None
if args.ninty:
    nyi = args.ninty
    dointy = True

if dointx or dointy or dointw:
    doint = True
    print("* Spectra will be interpolated!")
    print(dointx, dointy, dointw)

if args.trim:
    padi = 0
else:
    padi = 0.2



ftype = set()
fparna = []
flaba = []
xvec = []
for fname in fnames:
    
    ftype.add(fname.split(".",1)[0])
    fparna.append(fname.split(".",1)[1].split("_"))
    nform = hfileform(fname)
    f = open(fname)
    if nform:
        flaba.append(f.readline().split()[2:])
    
    x = []
    for line in f.readlines():
        if line.split() != []:
            if line.split()[0][0] == "#":
                i = line.split()[1]
                if i[-1] == "-":
                    i = 0.
                else:
                    i = float(''.join(i.split("_")[-1][3:]))
                x.append(i)
                if len(line.split()) > 2:
                    if nform:
                        print("Error! Inconsistent file form!")
                        sys.exit()
                    flaba.append(line.split()[2].split("_"))
    f.close()
    
    xvec.append(np.array(x))

if len(ftype) != 1:
    print("Error! Mixing files with different formats!")
    sys.exit()
else:
    ftype = next(iter(ftype))+"."

# Case where we can store all the data at the beginning
if args.loadall:
    
    print("* Loading all the data at once...")
    
    dtt = []
    for fname in fnames:
        try:
            dtt.append(np.loadtxt(fname))
        except:
            print("Warning! Some values cannot be converted to float!")
            print("Loading data using genfromtxt...")
            dtt.append(np.genfromtxt(fname,dtype=float))
        
    if not args.intcol is None:
        
        print("* Integrating over the columns...")
        sang = 1.
        dat = []
        for ifn in range(nf):
            if args.intcol == "sin":
                x = np.array(flaba[ifn],dtype=float)[[i-1 for i in cols]]
                sang = np.sin(x)

            nd, nc = dtt[ifn].shape
            dat = np.zeros((nd,2))
            for i in range(nd):
                dat[i,0] = dtt[ifn][i,0]
                zf = interp1d(x,dtt[ifn][i,cols]*sang, kind='cubic')
                dat[i,1] = fixed_quad(zf,x[0],x[-1],n=30)[0]
            
            flaba[ifn] = ["int"]
            dtt[ifn] = np.copy(dat)
        
        ncm = 1
        cols = [1 for i in range(nf)]

betalphas, thetas, phis = [], [], []
for icm in range(ncm):

    ifil = 0
    flabc = [set() for i in range(nf)]
    
    for ifn in range(nf):
        
        fname = fnames[ifn]
        
        test1d = False        
        icol = icm
        
        if flaba == []:
            flabc[ifil].add("")
        else:
            flabc[ifil].add(flaba[ifn][cols[icol]-1])
        
        if args.loadall:
            dat = dtt[ifn][:,[0,cols[icol]]]
            
        else:
            dat = np.loadtxt(fname, usecols=(0,cols[icol]))
            
        
        print(" ", ifil, " -> ", ops[ifil], fname, icol, cols[icol], end=" ")
        if facs != None:
            print(facs[ifil], end=" ")
        if ips != None:
            print(ips[ifil], end=" ")
        print()

        if np.isnan(dat).any():
            print("Warning! NaN detected in file ", fname)
            dat = dat[~np.isnan(dat).any(axis=1)]
            #sys.exit()

        nx = len(xvec[ifn])
        nd, nc = dat.shape
        ny = int(nd/nx)

        if nx*ny != nd:
            print("Error! Inhomogeneous file", fnames)
            print("nx, ny, nd = ", nx, ny, nd)
            sys.exit()

        #print(nd, nx, ny)
        yvec = np.zeros(ny)
        for j in range(ny):
            yvec[j] = dat[j,0]
        
        if ifil == 0:
            print("* nc0  = ", nc, "\t ic0 = ", cols[icol]-1)
            print("* nx0  = ", nx, "\t ny0  = ", ny)

            xarr = np.zeros((nx,ny))
            yarr = np.zeros((nx,ny))
            for i in range(nx):
                for j in range(ny):
                    xarr[i,j] = xvec[ifn][i]
                    yarr[i,j] = yvec[j]

            if not dointy and not dointw:
                nyi = ny
            if not dointx:
                nxi = nx
            if nyi == None:
                nyi = ny
            if nxi == None:
                nxi = nx

            zarr = np.zeros((nxi,nyi))
            xnew = np.copy(xarr)  # We copy so we do not need to test always if doint

            if doint:
                if dointy or dointw:
                    if dointw:
                        if args.fzeros:
                            ymin = min(ips)/au2ev+min(yarr[0,:])
                            ymax = max(ips)/au2ev+max(yarr[0,:])
                        else:
                            ymin = max(ips)/au2ev+min(yarr[0,:])
                            ymax = min(ips)/au2ev+max(yarr[0,:])
                            #print(max(ips),min(yarr[0,:]),min(ips),max(yarr[0,:]),ymin,ymax)
                            if ymin >= ymax:
                                print("Error! The IP differences are too large for the energy grid!")
                                sys.exit()
                    else:
                        ymin = min(yarr[0,:])
                        ymax = max(yarr[0,:])
                    ynew = np.repeat(np.linspace(ymin,ymax, num=nyi),nxi).reshape(nyi,nxi).transpose()
                else:
                    ynew = np.repeat(yarr[0,:], nxi).reshape(nyi,nxi).transpose()


                if dointx:
                    if args.xmin != None:
                        xmin = args.xmin
                    else:
                        xmin = np.min(xarr)

                    if args.xmax != None:
                        xmax = args.xmax
                    else:
                        xmax = np.max(xarr)
                    xnew = np.repeat(np.linspace(xmin,xmax, num=nxi),nyi).reshape(nxi,nyi)
                else:
                    xnew = np.repeat(xarr[:,0], nyi).reshape(nxi,nyi)
            
            if comp1d:
                zlabs = ["Total"]
                zcomp = np.zeros((nyi,1))
            
        else:
            
            #print(np.sort(yvec))
            #print(np.sort(dat[0:ny,0]))

            if nx == 1:
                print("Warning! File",fname," has nx=1. Assuming same spectrum for all time-delays...")
                test1d = True
                nx = nxi
                xvec[ifn] = xarr[:,0]
            else:
                if not dointx and not np.array_equal(np.sort(xvec[ifn]),np.sort(xarr[0:nx,0])):
                    print("Error! Some input files do not have the same x-grid:", fname)
                    #print(np.sort(xarr[0:nx,0]))
                    #print(np.sort(xvec))
                    sys.exit()
            
            if not dointy and not dointw:
                if not np.array_equal(np.sort(yvec),np.sort(yarr[0,0:ny])):
                    print("Error! Some input files do not have the same y-grid!")
                    #print(yvec-yarr[0,0:ny])
                    #print(np.sort(yarr[0,0:ny]))
                    #print(np.sort(yvec))
                    sys.exit()

        ztmp = np.zeros((nx,ny))
        k = 0
        for i in range(nx):
            # Case where we want to substract photoelectron spectra obtained with pump only (no time-delays)
            if test1d:
                k = 0
            for j in range(ny):
                if i != 0 and dat[k,0] != dat[j,0]:
                    print("Error! y-grid is inconsistent in ", fname)
                    print("i, j, k , dat[j,0], dat[k,0] = ", i, j, k, dat[j,0], dat[k,0])
                    sys.exit()
                ztmp[i,j] = dat[k,1]
                k += 1

        if doint:
            if dointw:
                if args.fzeros:
                    fz = interp2d(xvec[ifn], yvec+ips[ifil]/au2ev, ztmp.transpose(), kind='cubic',fill_value=0.0)
                else:
                    fz = interp2d(xvec[ifn], yvec+ips[ifil]/au2ev, ztmp.transpose(), kind='cubic')
            else:
                fz = interp2d(xvec[ifn], yvec, ztmp.transpose(), kind='cubic')
            ztmp = fz(xnew[:,0],ynew[0,:]).transpose()
        
        ztmp = facs[ifil]*ztmp
        zarr = opdic[ops[ifil]](zarr,ztmp)
        
        # Calculate the average/ select the time delay for specified component
        if comp1d:
            
            lab = fparna[ifn][comp1d].split(".")[0]
            if not lab in zlabs:
                zlabs.append(lab)
                zcomp = np.append(zcomp,np.zeros((nyi,1)),axis=1)
            i = zlabs.index(lab)
            
            if args.xmin != None:
                imin = np.argmax(xnew[:,0]>=args.xmin)
            else:
                imin = 0
            
            if args.xmax != None:
                imax = np.argmin(xnew[:,0]<=args.xmax)
            else:
                imax = nxi
            
            #print(args.xmin, args.xmax, imin, imax)
            #print(xnew[imin:imax,0])
            
            if test1d:
                zcomp[:,i] = opdic[ops[ifil]](zcomp[:,i], np.copy(ztmp[1,:]))
            else:
                if wsel == ["avg"]:
                    for j in range(nyi):
                        zf = interp1d(xnew[imin:imax,0],ztmp[imin:imax,j], kind='cubic')
                        zcomp[j,i] = opdic[ops[ifil]](zcomp[j,i], fixed_quad(zf,xnew[imin,0],xnew[imax-1,0],n=30)[0]/(xnew[imax-1,0]-xnew[imin,0]))   # We apply operations on data with same label only
                elif wsel != None:
                    if ifil == 0:
                        print("* Selected time delay (fs):", xnew[wsel[0],0])
                    for j in range(nyi):
                        zcomp[j,i] = opdic[ops[ifil]](zcomp[j,i], ztmp[wsel[0],j])   # We apply operations on data with same label only
                
        ifil += 1

    if flabc[0]:
        clab = next(iter(flabc[0])).lower()
        for i in flabc:
            if len(i) > 1:
                print("Error! Inconsistent column labels!")
                sys.exit()
            if clab != next(iter(i)).lower():
                clab = "-"
    else:
        clab = "-"

    if ftype == "psig." or ftype == "pexc." or ftype == "rexc.":
        tlab = "   Symmetry: "+clab
    elif ftype == "pang." or ftype == "opang." or ftype == "ipang." or ftype == "panx." or ftype == "opanx." or ftype == "ipanx.":
        tlab = r'   $(\theta,\phi)$ = ('+clab+")"
    elif ftype == "pvib.":
        tlab = "   v' = "+clab
    else:
        print("Error! Bad file type!")
        sys.exit()
    
    prif = ""
    if ftype[0] == "o":
        prif = "o"
    if ftype[0] == "i":
        prif = "i"

    nx = nxi
    ny = nyi
    print("* nxi  = ", nx, "\t nyi  = ", ny)

    if doint:
        xarr = np.copy(xnew)
        yarr = np.copy(ynew)


    arrinds = xarr[:,0].argsort()
    xarr = xarr[arrinds]
    yarr = yarr[arrinds]
    zarr = zarr[arrinds]

    yarr = yarr*au2ev

    if args.sgf:
        wsg = int(ny/args.sgf)
        wsg = wsg+1-(wsg%2)*1
        for i in range(nx):
            zarr[i,:] = savgol_filter(zarr[i,:],wsg , 7)

    if emom:
        yarr = np.sqrt(yarr*2./au2ev)

    if not dointw and epho:
        yarr = yarr+ipmin

    if args.xmin != None:
        xmin = args.xmin
        imin = np.argmax(xarr[:,0]>=args.xmin)
    else:
        xmin = np.min(xarr)
        imin = 0

    if args.xmax != None:
        xmax = args.xmax
        imax = np.argmin(xarr[:,0]<=args.xmax)
    else:
        xmax = np.max(xarr)
        imax = nx

    if args.ymin != None:
        ymin = args.ymin
    else:
        ymin = np.min(yarr)

    if args.ymax != None:
        ymax = args.ymax
    else:
        ymax = np.max(yarr)
    
    print(imin, imax)
    nx = imax-imin
    print()
    print("* New nx = ", nx)
    print("* xmin = ", xmin, "\t xmax = ", xmax)
    print("* ymin = ", ymin, "\t ymax = ", ymax)
    
    # We filter the values outside the range [xmin:xmax]
    xarr = xarr[imin:imax,:]
    yarr = yarr[imin:imax,:]
    zarr = zarr[imin:imax,:]

    zmin = 1e6
    zmax = -1e6
    for i in range(nx):
        for j in range(ny):
            if yarr[i,j] >= ymin and yarr[i,j] <= ymax:
                if zarr[i,j] < zmin:
                    zmin = zarr[i,j]
                if zarr[i,j] > zmax:
                    zmax = zarr[i,j]
    
    if args.zsym:
        if np.abs(zmin) > zmax:
            zmax = np.abs(zmin)
        else:
            zmin = -zmax
    
    print()
    print("* zmin = ", zmin, "\t zmax = ", zmax)

    if args.vmin != None:
        vmin = args.vmin
    else:
        vmin = zmin

    if args.vmax != None:
        vmax = args.vmax
    else:
        vmax = zmax

    print("* vmin = ", vmin, "\t vmax = ", vmax)
    
    if comp1d:
        zcomp[:,0] = np.sum(zcomp[:,1:], axis=1)
        
        if len(zlabs) == 2:
            zlabs = [zlabs[1]]
            zcomp = zcomp[:,1].reshape((nyi,1))

    
    fldname = fparna[0]
    no = len(fldname)
    for i in range(1,nf):
        nu = len(fparna[i])
        if nu != no:
            print("Warning! Inconsistent input filenames!")
        for j in range(1,min(nu,no)):
            if fparna[i][j] != fldname[j]:
                if j == 4 and fparna[i][j].split(".")[0] == fldname[j].split(".")[0]:
                    fldname[j] = fldname[j].split(".")[0]
                else:
                    fldname[j] = "-"
    
    betalphas.append(fldname[3])
    
    if args.oname:
        fldname = args.oname.split("_")
        fldname[0] = fldname[0].replace(ftype,"")
        nu = len(fldname)
        if nu != no:
            print("Error! Inconsistent oname paramater:", no, nu)
            sys.exit()
        
    
    genname = "_".join(fldname)
    outname = genname[:]
    if clab != "":
        outname = outname+"_"+clab.replace(",","-")
    print("\n* Outname: "+outname+"\n")
    
    
    if args.exp:

        if epho:
            print("Warning! Exporting photoelectron spectra as a function of photon energy!")
        
        if comp1d:
            f = open(ftype+outname+"_c1d","w")
            print("# Labels: ", *zlabs, file=f)
            for j in range(ny):
                print(yarr[0,j]/au2ev, *zcomp[j,:], file=f)
                #print(yarr[0,j]/au2ev, np.sum(zarr[:,j])/nx, file=f)
            f.close()
        else:
            
            if icm == 0:
                mlabs = []
                marr = []
            mlabs.append(clab)
            marr.append(zarr)
    
    
    if doplot:
        fig, axarr = plotinit()


    if nfft:
        k = 0
        nfh = nfft/2
        if nfh % 2 == 0:
            nfh = nfh+1
        zfft = np.zeros(nfh)
        xgrid = np.linspace(xmin, xmax, num=nfft) # Check why nfft
        for j in range(ny):
            k += 1
            if yarr[0,j] >= ymin and yarr[0,j] <= ymax:
                zf = interp1d(xarr[:,j],zarr[:,j], kind='cubic')
                zfft = zfft + np.fft.rfft(zf(xgrid))
            zfft = zfft/k
            omega = np.fft.fftfreq(nfft,d=(xgrid[1]-xgrid[0])*femt2au)*2.*np.pi*au2ev

            f = open(prif+"pfft."+outname,"w")
            for i in range(nfft/2):
                print(omega[i], np.sqrt(zfft[i].real**2+zfft[i].imag**2), file=f)
            f.close()

    if wsel != None:
        
        if args.wlf:
            eord, epos = posbands(yarr[0,:], ips[0], wf, iband=args.iband, epho=epho)
            ttstr = btstr
        else:
            epos = list(range(nyi))
            ttstr = ""
            #print(bord, bpos, wsel, eint)
        
        for k in range(nws):
            if icm == 0:
                if wsel[k] == "avg":
                    fws[k] = open(ftype.replace(".","_")+genname+"_"+ttstr+"wavg.dat","w")
                else:
                    fws[k] = open(ftype.replace(".","_")+genname+"_"+ttstr+"w"+str(wsel[k]).zfill(3)+".dat","w")
                
                if ncm != 1:
                    if args.wlf:
                        print("# band-orders:", *bord, file=fws[k])
                    else:
                        print("# Ee(eV):", *yarr[0,:], file=fws[k])
            
            if wsel[k] == "avg":
                if ncm != 1:
                    print(clab.replace(","," "), end=" ", file=fws[k])
                for j in epos:
                    zf = interp1d(xarr[:,0],zarr[:,j], kind='cubic')
                    i = fixed_quad(zf,xarr[0,0],xarr[-1,0],n=30)[0]/(xarr[-1,0]-xarr[0,0])
                    if ncm == 1:
                        print(yarr[0,j], i, file=fws[k])
                    else:
                        print(i, end=" ", file=fws[k])
                print(file=fws[k])
                #print(clab.replace(","," "), *[np.sum(zarr[:,j])/nx for j in bpos], file=fws[k])
            else:
                if ncm == 1:
                    for j in epos:
                        print(j)
                        print(yarr[0,j], zarr[wsel[k],j], file=fws[k])
                else:
                    print(clab.replace(","," "), *[zarr[wsel[k],j] for j in epos], file=fws[k])
        
    
    rects, selbands = [[], []], []
    nfl = 0
    if eint != None:

        bord, benr, bpos, selbands, eflims, iflims = findbands(yarr[0,:], ipmin, wf, args.iband, eint, epho=epho)
        if args.tper:
            selbands = filtbands(iflims, zarr, args.tper)
        if sbor != [0]:
            selbands = maskbands(bord, selbands, sbor)
        
        nfl = len(eflims[0])
        
        if doplot:
            for j in range(nfl):
                #print(bord[j],eflims[0][j],eflims[1][j])
                if comp1d:
                    drawsb(j,iax=1)
                for i in range(2):
                    if args.axflip:
                        axarr[0].axvline(x=eflims[i][j],color='gray', linewidth=1.5, linestyle="dashed")
                    else:
                        axarr[0].axhline(y=eflims[i][j],color='gray', linewidth=1.5, linestyle="dashed")


    if doplot:
        if args.auto:
            rects[0] = []
            for j in selbands:
                drawsb(j)
        else:
            selbands = []
            cid = fig.canvas.mpl_connect('button_press_event', on_click)
            #cid = fig.canvas.mpl_connect('key_press_event', on_key)

        if args.png:
            plt.savefig(ftype+outname+".png",bbox_inches='tight', pad_inches=padi, dpi=200)
        else:
            if args.auto:
                plt.pause(1)
            else:
                plt.show()

    nb = len(selbands)
    if nb > 0:
        amp, zint, popts, apsd = fitbands(xarr[:,0], yarr[0,:], zarr, selbands, eflims, iflims, args.env, ff, frq, ffrq=args.ffrq,mode=args.fmode,fde=args.fde,doplot=doplot)
        #print(popts)
        if doplot:
            if args.auto:
                plt.close()
            fig.clf()
            fig, axarr = plotinit()

            if not args.png:

                for l in range(nb):
                    j = selbands[l]
                    par0 = np.copy(popts[l,:])
                    par0[2] = benr[j]
                    amp0 = eflims[1][j]-eflims[0][j]+benr[j]
                    axarr[0].plot(xarr[:,0],fitfunc(xarr[:,0], "-", ff, amp0, frq, *par0), color="white", linewidth=4.0, linestyle='-')

            #plotsbo()
        
        f = open("sbfits.dat", "w")
        for l in range(nb):
            for i in range(nx):
                print(xarr[i,0], zint[l,i], fitfunc(xarr[i,0], args.env, ff, amp[l], frq, *popts[l,:]), file=f)
            print(file=f)
        f.close()

        if args.phs:
            pref = "ph"
            pfac = 1.
        else:
            pref = "td"
            pfac = 1e3/(femt2au*2.*wf/au2ev)
        
        if icm == 0:
            if ncm == 1:
                midname = outname
            else:
                midname = genname
            if args.pso:
                ft = open(prif+pref+"so."+midname+"_f"+args.fmode[0]+args.eint,"w")
                fp = open(prif+"pdso."+midname+"_f"+args.fmode[0]+args.eint,"w")
                boe = bord
                bstr = "band_order"
            else:
                ft = open(prif+pref+estr+midname+"_f"+args.fmode[0]+args.eint,"w")
                fp = open(prif+"pd"+estr+midname+"_f"+args.fmode[0]+args.eint,"w")
                boe = benr
                if epho:
                    bstr = "Ep(eV)"
                else:
                    bstr = "Ee(eV)"
        
        betalpha = list(set(betalphas))
        beta, alpha = "-", "-"
        if len(betalpha) == 1:
            try:
                beta, alpha = betalpha[0].split("-")
            except:
                beta, alpha = "-", "-"
        
        if ncm == 1:
            wtdrab(icm, boe, bstr, selbands, popts, apsd, ft, fp, pfac=pfac, beta=beta, alpha=alpha, ioner=ioner, reb=args.reb)
        else:
            xang = clab.split(",")
            try:
                xang = [float(i)*deg2rad for i in xang]
                thetas.append(xang[0])
                if len(xang) == 1:
                    phis = None
                else:
                    if prif == "o":
                        phis = [xang[1]]
                    else:
                        phis.append(xang[1])
            except:
                thetas = None
                phis = None
                
            wtdrab(icm, boe, bstr, selbands, popts, apsd, ft, fp, pfac=pfac, beta=beta, alpha=alpha, clab=clab, ioner=ioner, theta=thetas, phi=phis, reb=args.reb)

        if args.amp:
            pref = "ap"
            f = open(prif+pref+estr+outname+"_de"+args.eint,"w")
            for l in range(nb):
                print(benr[selbands[l]], amp[l], file=f)
            print(file=f)
            f.close()

    if doplot:
        if args.png:
            plt.savefig(ftype+outname+".png",bbox_inches='tight', pad_inches=padi, dpi=200)
            plt.close()
        else:
            if args.auto:
                plt.pause(2)
                plt.close()
            else:
                plt.show()

if args.exp and not comp1d:
    
    fname = ftype+genname
    if os.path.isfile(fname):
        print("Warning! Cannot export "+fname+" because the file exists!")
    else:
        f = open(fname,"w")
        if not ioner is None:
            print("# ", ioner, *mlabs, file=f)
        for i in range(nx):
            if ioner is None:
                print("# s1-"+str(xarr[i,0]), "_".join(mlabs), file=f)
            else:
                print("# s1-"+str(xarr[i,0]), file=f)
            for j in range(ny):
                print(("{:13.5e}"*(ncm+1)).format(yarr[0,j]/au2ev, *[marr[icm][i,j] for icm in range(ncm)]), file=f)
            print(file=f)
        f.close()
