# Readme

## About plotspec

**plotspec** is a Python code to plot, fit and analyze theoretical/experimental photoelectron specra as a function of two variables. It can perform mathematical operations on several spectra and plot/export the resulting spectrum.
It also has the ability to fit the sidebands of RABBITT (Reconstruction of Attosecond harmonic Beating by Interference of Two-photon Transitions) spectra when given as a function of photoelectron energy and pump-probe time delay by using the following formula:

```math
S_{2q}=\alpha+\beta cos\left[2\omega\tau - \Delta\phi_{2q}-\Delta\phi_{at}\right]
```

**Example of spectra obtained with plotspec by subtracting the XUV-only spectrum from the XUV+IR (RABBITT spectrum) for a particular experimental setup:**
![](examples/rabbit-spectrum_n2.png)

## Installation

Requirements:
1. Python 3.0 and higher versions
2. Numpy
3. Matplotlib
4. Scipy

## Inputs

The format of the input files must be as follow:
- 1st line: # <IP value in a.u.> <Label column 2> <Label column 3> ...
- 2nd line: # <1st value of the first variable>
- 3rd line: <1st value of the second variable> <Value column 2> <Value column 3> ...
- 4th line: <2nd value of the second variable> <Value column 2> <Value column 3> ...
- ...
- nth line: <nth value of the second variable> <Value column 2> <Value column 3> ...
- (n+1)th line: <blank line>
- (n+2)th line: # <2nd value of the first variable>
- (n+3)th line: <1st value of the second variable> <Value column 2> <Value column 3> ...
- ...

## Licence

This code is under GNU General Public License (GPL) version 3
